# Dans quel mesure ais-je utilisé le flex ?
J'ai utilisé le flex dans toutes les pages a different endroit pour differente chose, il m'a notamment permis :

- De placer où je voulais mes texte,images (gauche,milieu,droite)
- De modifier la taille de certains elements :
- - Pour qu'il prenne toute la place disponnible ou non, qu'il s'ajuste aux autres éléments
- - Pour les placer cote a cote, mais aussi les uns en dessous des autres

# Travail supplémentaires qui m'a demander des recherches
 ## HTML
J'ai tout d'abord appris a implémenter des iframes, choses assez simple et intuitive. J'ai aussi pris un tableau opensource de wikipedia et implémentez, j'ai mis pas mal de temps a le comprendre et faire quelque modifications afin de faire son css et le rendre responsive.

## CSS
J'ai aprris plusieurs choses en CSS durant ce mini-projet :
d'abord les @media screen qui me permette d'effectuer des règles seulement quand la fenêtre fait une certain taille, ça m'a aidé pour le responsive et meme pour rendre le site plus beau quand il était plus petit.
J'ai aussi eu l'occasion de travailler avec les class, notamment pour rendre les iframes responsive, les faire garder le ratio 16:9 (cf commentaire).


## problème :
Pour rendre les pages responsive, j'ai testé de deux manière:

- moduler directement la taille de la fenêtre, ici aucun problème le site est bien responsive

- transformer mon google comme un télèphone avec l'inspecteur élèment, la j'ai eu des problemes, la plupart des résolution télèphones donne a mon site une allure très bizarre ( que je n'ai jamais obtenue en modulant la taille de la fenêtre ) et un seul fonctionne comme la modulation de fenêtre.

# Mon gitlab
https://gitlab.com/KorentinGeorget